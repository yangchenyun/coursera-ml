function [J grad] = nnCostFunction(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs classification
%   [J grad] = NNCOSTFUNCTON(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);
         
% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

% ====================== YOUR CODE HERE ======================
% Instructions: You should complete the code by working through the
%               following parts.
%
% Part 1: Feedforward the neural network and return the cost in the
%         variable J. After implementing Part 1, you can verify that your
%         cost function computation is correct by verifying the cost
%         computed in ex4.m
%
% Part 2: Implement the backpropagation algorithm to compute the gradients
%         Theta1_grad and Theta2_grad. You should return the partial derivatives of
%         the cost function with respect to Theta1 and Theta2 in Theta1_grad and
%         Theta2_grad, respectively. After implementing Part 2, you can check
%         that your implementation is correct by running checkNNGradients
%
%         Note: The vector y passed into the function is a vector of labels
%               containing values from 1..K. You need to map this vector into a 
%               binary vector of 1's and 0's to be used with the neural network
%               cost function.
%
%         Hint: We recommend implementing backpropagation using a for-loop
%               over the training examples if you are implementing it for the 
%               first time.
%
% Part 3: Implement regularization with the cost function and gradients.
%
%         Hint: You can implement this around the code for
%               backpropagation. That is, you can compute the gradients for
%               the regularization separately and then add them to Theta1_grad
%               and Theta2_grad from Part 2.
%

% add the bias to input, and compute the activation at layer 1
A1 = [ones(size(X, 1), 1), X];
Z2 = A1 * Theta1';
A2 = [ones(size(Z2, 1), 1), sigmoid(Z2)];
Z3 = A2 * Theta2';
A3 = sigmoid(Z3);

Y = zeros(size(y, 1), num_labels);
for row = 1:size(y, 1)
  Y(row, y(row)) = 1;
end

J = 0;
for mi = 1:m
  for ki = 1:num_labels
    J = J + Y(mi, ki)' * log(A3(mi, ki)) + (1 - Y(mi, ki)') * log(1-A3(mi, ki));
  end
end
J = -1/m * J;

regCost = 0;
for j = 1:hidden_layer_size
  for k = 2:(input_layer_size + 1) % ignore the bias weight
    regCost = regCost + Theta1(j, k) ^ 2;
  end
end

for j = 1:num_labels
  for k = 2:(hidden_layer_size + 1) % ignore the bias weight
    regCost = regCost + Theta2(j, k) ^ 2;
  end
end

J = J + lambda / (2 * m) * regCost


% Back propagation algorithm
for mi = 1:m
  % compute using column vector
  a1 = A1(mi, :)';
  z2 = Z2(mi, :)';
  a2 = A2(mi, :)';
  z3 = Z3(mi, :)';
  a3 = A3(mi, :)';
  yi = Y(mi, :)';

  delta_3 = a3 - yi;
  delta_2 = (Theta2' * delta_3) .* [1; sigmoidGradient(z2)];
  delta_2 = delta_2(2:end);

  Theta1_grad = Theta1_grad + delta_2 * a1';
  Theta2_grad = Theta2_grad + delta_3 * a2';
end

% In regularization, the theta for bias is not counted
Theta1(:, 1) = 0;
Theta2(:, 1) = 0;

Theta1_grad = (1/m) * Theta1_grad + (lambda / m) * Theta1;
Theta2_grad = (1/m) * Theta2_grad + (lambda / m) * Theta2;


% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end

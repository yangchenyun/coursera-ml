function J = computeCost(X, y, theta)
%COMPUTECOST Compute cost for linear regression
%   J = COMPUTECOST(X, y, theta) computes the cost of using theta as the
%   parameter for linear regression to fit the data points in X and y

% Initialize some useful values
m = length(y); % number of training examples

% You need to return the following variables correctly 
J = 0;

% ====================== YOUR CODE HERE ======================
% Instructions: Compute the cost of a particular choice of theta
%               You should set J to the cost.
% for i = 1:m
%   Xi = X(i, :);
%   yi = y(i);
%   h_theta = Xi * theta;
%   % With logistic cost function
%   % h_theta = 1 / (1 + exp(1)^(Xi * theta));
%   Ji = 1 / (2 * m) * (h_theta - yi)^2;
%   J += Ji;
% end

% Vectorized implementation
V = X * theta - y;
J = 1 / (2 * m) * V' * V;



% =========================================================================

end
